const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const port =  process.env.PORT || 45081

const router = express.Router();
const appRoutes = require('./routes/api')(router);

const { Kafka } = require('kafkajs');
const kafka = new Kafka({
  clientId: 'ywc-bargin-response',
  brokers: ['localhost:9498']
});

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// enable All CORS Requests
app.use(cors());

const topic = 'BARGAIN';

const consumer = kafka.consumer({ groupId: 'ywc-group-req' })
const producer = kafka.producer();

app.use((req, res, next) => {
  req.producer = producer;
  return next();
});

app.use('/api', appRoutes);

async function run() {
  await consumer.connect()
  await consumer.subscribe({ topic })

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      console.log({
            partition,
            offset: message.offset,
            value: message.value.toString(),
          })
    },
  });

  app.listen(port, () => {
  	console.log(`server is successfully running on port ${port}` );
  });
}
run().catch(console.error);
