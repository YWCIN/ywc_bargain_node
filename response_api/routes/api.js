const { Client } = require('@elastic/elasticsearch');
const client = new Client({ node: 'http://localhost:9222' });

module.exports = (router) => {

  router.post('/vendorResponse', async(req,res) => {
    try {
      const topic = req.body.title;
      await req.producer.connect();
      await req.producer.send({
        topic: topic,
        messages: [
          { value: JSON.stringify(req.body) },
        ],
      });

      // Let's start by indexing data to elasticsearch
      await client.index({
        index: 'ywc_bargain',
        body: {
          title: req.body.title,
          uid: req.body.uid,
          event_saga_store_id: req.body.event_saga_store_id,
          vendor_ID: req.body.VNDR_ID,
          merchant_ID: req.body.MRCNT_ID,
          merchant_STORE_ID: req.body.MECNT_STORE_ID,
          manufacture_ID: req.body.MNFCTR_ID,
          customer_ID: req.body.CUST_ID,
          customer_Name: req.body.CUST_NAME,
          product_ID: req.body.PRODUCT_ID,
          product_image_url: req.body.PRODUCT_IMAGE_URL,
          sku_ID: req.body.SKU_ID,
          upc: req.body.UPC,
          quantity: req.body.QUANTITY,
          bargin_PRICE: req.body.BRG_PRICE,
          status: req.body.STATUS,
          sku_DESC: req.body.SKU_DESC,
          category : req.body.CATEGORY,
          sub_category: req.body.SUB_CATEGORY,
          sku_name: req.body.SKU_NAME,
          manufacture: req.body.MANUFACTURER,
          ywc_ID:req.body.YWC_ID,
          offering_PRICE : req.body.OFFERING_PRICE,
          lowest_PRICE: req.body.LOWEST_PRICE,
          merchant: req.body.MERCHANT,
          vendor: req.body.VENDOR,
          active_status: true,
          datetime: new Date()
        }
      });

      await client.update({
        index: 'ywc_bargain',
        id: req.body.docId,
        body: {
          doc: {
            active_status: false 
          }
        }
      });

      // here we are forcing an index refresh, otherwise we will not get any result in the consequent search
      await client.indices.refresh({ index: 'ywc_bargain' });

      res.json({ message: 'Response sent Successfully'});

    } catch (error) {
      console.log(error);
    }
  });

  return router;
}
