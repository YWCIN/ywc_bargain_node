const express = require('express');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const port =  process.env.PORT || 32431

const { Client } = require('@elastic/elasticsearch');
const client = new Client({ node: 'http://localhost:9222' });

const { Kafka } = require('kafkajs');
const kafka = new Kafka({
  clientId: 'ywc-bargin-request',
  brokers: ['localhost:9498']
});

const producer = kafka.producer();
const consumer = kafka.consumer({ groupId: 'ywc-group' })

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());

// enable All CORS Requests
app.use(cors());

async function run() {
  // Consuming
  await consumer.connect();
  await consumer.subscribe({ topic: 'ACCEPT' });
  await consumer.subscribe({ topic: 'REJECT' });
  await consumer.subscribe({ topic: 'NOTIFICATION' });

  await consumer.run({
    eachMessage: async ({ topic, partition, message }) => {
      console.log({
        partition,
        offset: message.offset,
        value: message.value.toString(),
      })
    },
  });
}
run().catch(console.error);

app.post('/bargainRequest', async(req,res) => {
  try {   
    const checkElasticForDuplicate = await client.search({
      index: 'ywc_bargain',
      "body": {
        "query": {
          "bool": {
            "must":[
              {
                "match": { "vendor_ID": req.body.VNDR_ID }
              },
              {
                "match": { "sku_ID": req.body.SKU_ID }
              },
              {
                "match": { "bargin_PRICE": req.body.BRG_PRICE }
              }
            ]
          }
        }
      }
    }).then(function(response) {
      return response.body.hits.total.value;
    });

    if(checkElasticForDuplicate !== 0 ) {
      res.json({ message: 'Bargain is already submitted for this price please start bargain with another price'});
    } else {

      const topic = 'BARGAIN'; 

      // Producing
      await producer.connect();
      await producer.send({
        topic: topic,
        messages: [
          { value: JSON.stringify(req.body) },
        ],
      });

      // Let's start by indexing data to elasticsearch
      await client.index({
        index: 'ywc_bargain',
        body: {
          title: req.body.title,
          uid: req.body.uid,
          event_saga_store_id: req.body.event_saga_store_id,
          vendor_ID: req.body.VNDR_ID,
          merchant_ID: req.body.MRCNT_ID,
          merchant_STORE_ID: req.body.MECNT_STORE_ID,
          manufacture_ID: req.body.MNFCTR_ID,
          customer_ID: req.body.CUST_ID,
          customer_Name: req.body.CUST_NAME,
          product_ID: req.body.PRODUCT_ID,
          product_image_url: req.body.PRODUCT_IMAGE_URL,
          sku_ID: req.body.SKU_ID,
          upc: req.body.UPC,
          quantity: req.body.QUANTITY,
          bargin_PRICE: req.body.BRG_PRICE,
          status: req.body.STATUS,
          sku_DESC: req.body.SKU_DESC,
          category : req.body.CATEGORY,
          sub_category: req.body.SUB_CATEGORY,
          sku_name: req.body.SKU_NAME,
          manufacture: req.body.MANUFACTURER,
          ywc_ID:req.body.YWC_ID,
          offering_PRICE : req.body.OFFERING_PRICE,
          lowest_PRICE: req.body.LOWEST_PRICE,
          merchant: req.body.MERCHANT,
          vendor: req.body.VENDOR,
          active_status: req.body.ACTIVE_STATUS,
          datetime: new Date()
        }
      });

      // here we are forcing an index refresh, otherwise we will not get any result in the consequent search
      await client.indices.refresh({ index: 'ywc_bargain' });

      res.json({ message: 'Your bargain request sent successfully'});      
    }
  } catch (error) {
    console.log(error);
  }
});

app.listen(port, () => {
	console.log(`server is successfully running on port ${port}` );
});
